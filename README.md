
**Code challenge Register Page**

This is the code challenge To 'CREATE AN ACCOUNT' in automationpractice.com site.

**Installation**

Should be available Node Js.

 
1. Install Selenium-webdriver using npm:
 
2. "npm install selenium-webdriver"

3. Install Jest using npm:

4. "npm install --save-dev jest"
 
5. Use "npm run test" to run.

**Usage**

>Before moving with the test cases, we need to gather some requirements like:

Gender
First Name
Last Name
User Name
Password
Email ID
Phone Number
Address

>Selenium Form WebElement

Text fields
Buttons
Radio buttons
Checkboxe