const chromeDriver = require("../drivers/chrome");
const {Builder, By, Key, util} = require("selenium-webdriver");
var webdriver = require('selenium-webdriver'),
    SeleniumServer = require('selenium-webdriver/remote').SeleniumServer,
    request = require('request');

describe("Aura Code Challenge - Create User Account Tests", () => {
  let driver;
  beforeAll(async () => {
    driver = await new Builder().forBrowser("chrome").build();
     driver.manage().window().maximize();
  });

  afterAll(async () => {
    await driver.quit();
  });

  test("it loads authentication page", async () => {
    await driver.get(
      "http://automationpractice.com/index.php?controller=authentication&back=my-account"
    );     
            driver.manage().setTimeouts( { implicit: 5000 } )
            await driver.findElement(webdriver.By.xpath("//input[@id='email_create']")).sendKeys("testuserchallange2.kbp9@gmail.com");
            driver.manage().setTimeouts( { implicit: 5000 } )
            await driver.findElement(webdriver.By.xpath("//body/div[@id='page']/div[2]/div[1]/div[3]/div[1]/div[1]/div[1]/form[1]/div[1]/div[3]/button[1]/span[1]")).click();
            driver.manage().setTimeouts( { implicit: 5000 } )
            await driver.findElement(webdriver.By.xpath("//input[@id='customer_firstname']")).sendKeys("bhavaniprasad");
            driver.manage().setTimeouts( { implicit: 5000 } )
            await driver.findElement(webdriver.By.xpath("//input[@id='customer_lastname']")).sendKeys("kanna");
            driver.manage().setTimeouts( { implicit: 5000 } )
            await driver.findElement(webdriver.By.xpath("//input[@id='address1']")).sendKeys("Nethajinagar");
            driver.manage().setTimeouts( { implicit: 5000 } )
            await driver.findElement(webdriver.By.xpath("//input[@id='city']")).sendKeys("Repalle");
            driver.manage().setTimeouts( { implicit: 5000 } )
            await driver.findElement(webdriver.By.css("#id_state>option[value='5']")).click();
            driver.manage().setTimeouts( { implicit: 5000 } )
            await driver.findElement(webdriver.By.xpath('(//input[@type="text"])[11]')).sendKeys("94105");
            driver.manage().setTimeouts( { implicit: 5000 } )
            await driver.findElement(webdriver.By.xpath('//input[@id="phone_mobile"]')).sendKeys("9999999999");
            driver.manage().setTimeouts( { implicit: 5000 } )
            await driver.findElement(webdriver.By.css("#passwd")).sendKeys("bh@v@nKBP");
            driver.manage().setTimeouts( { implicit: 5000 } )
            await driver.findElement(webdriver.By.css("body.authentication.hide-left-column.hide-right-column.lang_en:nth-child(2) div.columns-container div.container div.row:nth-child(3) div.center_column.col-xs-12.col-sm-12 form.std.box div.submit.clearfix:nth-child(4) button.btn.btn-default.button.button-medium:nth-child(4) > span:nth-child(1)")).click();
			
			// Account page title validation
            const title = await driver.getTitle();
            expect(title).toBe("My account - My Store");
  });

});
